package com.example.GasPreisRechner.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class GasWerte {

    @Id
    private int postleitzahl;

    private String ort;

    private String ortsteil;

    private double brennwert;

    private double zustandszahl;

    public int getPostleitzahl() {
        return postleitzahl;
    }

    public String getOrt() {
        return ort;
    }

    public String getOrtsteil() {
        return ortsteil;
    }

    public double getBrennwert() {
        return brennwert;
    }

    public double getZustandszahl() {
        return zustandszahl;
    }

    public void setPostleitzahl(int postleitzahl) {
        this.postleitzahl = postleitzahl;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public void setOrtsteil(String ortsteil) {
        this.ortsteil = ortsteil;
    }

    public void setBrennwert(double brennwert) {
        this.brennwert = brennwert;
    }

    public void setZustandszahl(double zustandszahl) {
        this.zustandszahl = zustandszahl;
    }

    public GasWerte(int postleitzahl, String ort, String ortsteil, double brennwert, double zustandszahl) {
        this.postleitzahl = postleitzahl;
        this.ort = ort;
        this.ortsteil = ortsteil;
        this.brennwert = brennwert;
        this.zustandszahl = zustandszahl;
    }

    public GasWerte(){

    }
}
