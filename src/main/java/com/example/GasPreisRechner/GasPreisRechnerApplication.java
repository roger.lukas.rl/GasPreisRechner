package com.example.GasPreisRechner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GasPreisRechnerApplication {

	public static void main(String[] args) {
		SpringApplication.run(GasPreisRechnerApplication.class, args);
	}

}
