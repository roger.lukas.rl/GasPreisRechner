package com.example.GasPreisRechner.repository;

import com.example.GasPreisRechner.entity.GasWerte;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GasWerteRepository {

    public static List<GasWerte> findAll(Integer postleitzahl){

        List<GasWerte> gaswerteAuslesen = Arrays.asList(
            new GasWerte(
                    13055,
                    "Berlin",
                    "Hohenschönhausen",
                    11.0,
                    0.95
            ),
            new GasWerte(
                    15366,
                    "Brandenburg",
                    "Hoppegarten",
                    11.0,
                    0.95
            ),
            new GasWerte(
                    13056,
                    "Berlin",
                    "Hohenschönhausen",
                    11.0,
                    0.95
            )
        );


        if (postleitzahl == null){
            return gaswerteAuslesen;
        } else {
            List<GasWerte> filtered = new ArrayList<>();

            for (GasWerte g : gaswerteAuslesen){
                if (g.getPostleitzahl() == (postleitzahl)){
                    filtered.add(g);
                }
            }
            return filtered;
        }
    }
}
