package com.example.GasPreisRechner.repository;

import com.example.GasPreisRechner.entity.GasWerte;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Arrays;
import java.util.List;

public interface WerteRepository extends JpaRepository<GasWerte, Integer> {
}
