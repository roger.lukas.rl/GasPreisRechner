package com.example.GasPreisRechner.controller;

import com.example.GasPreisRechner.entity.GasWerte;
import com.example.GasPreisRechner.repository.GasWerteRepository;
import com.example.GasPreisRechner.repository.WerteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class GasPreisRechnerController {

    @Autowired
    WerteRepository werteRepository;

    public GasPreisRechnerController(WerteRepository werteRepository){
        this.werteRepository = werteRepository;
    }

    @GetMapping("/values/{postleitzahl}")
    public GasWerte gefilterteWerte(@PathVariable("postleitzahl") Integer postleitzahl){
        return werteRepository.findById(postleitzahl).get();
    }

    @GetMapping("/values")
    public List<GasWerte> alleGasWerte(
            @RequestParam(required = false) Integer postleitzahl
    ){
        return werteRepository.findAll();
    }

    @GetMapping("/local-values") // http://localhost:8080/locale-werte?postleitzahl=13055
    public List<GasWerte> index(
            @RequestParam(required = false) Integer postleitzahl
    ){
        return GasWerteRepository.findAll(postleitzahl);
    }

    @PostMapping("/add-values")
    public GasWerte addValues(@RequestBody GasWerte gw){
        return werteRepository.save(gw);
    }

    @PutMapping("/change-brennwert/{postleitzahl}/{brennwert}")
    public GasWerte changeValuesOfBrennwert(@PathVariable(name = "postleitzahl") int postleitzahl, @PathVariable(name = "brennwert") double brennwert) throws Exception {
        Optional<GasWerte> gaswerte = werteRepository.findById(postleitzahl);

        if(gaswerte.isPresent()){
            GasWerte gw = gaswerte.get();
            gw.setBrennwert(brennwert);
            return werteRepository.save(gw);
        } else {
            throw new Exception("Postleitzahl existiert noch nicht im System");
        }
    }

    @PutMapping("/change-zustandszahl/{postleitzahl}/{zustandszahl}")
    public GasWerte changeValuesOfZustandszahl(@PathVariable(name = "postleitzahl") int postleitzahl, @PathVariable(name = "zustandszahl") double zustandszahl) throws Exception {
        Optional<GasWerte> gaswerte = werteRepository.findById(postleitzahl);

        if(gaswerte.isPresent()){
            GasWerte gw = gaswerte.get();
            gw.setZustandszahl(zustandszahl);
            return werteRepository.save(gw);
        } else {
            throw new Exception("Postleitzahl existiert noch nicht im System");
        }
    }

  //  @PutMapping("/calc/GET?{postleitzahl}&{consume}&{priceInCent}")
  //  public GasWerte calc(@PathVariable(name = "postleitzahl") int postleitzahl, @PathVariable(name = "consume") double consume, @PathVariable(name = "priceInCent") double priceInCent) throws Exception {
   //     Optional<GasWerte> gaswerte = werteRepository.findById(postleitzahl);

   //     if(gaswerte.isPresent()){
    //        GasWerte gw = gaswerte.get();
   //         gw.setZustandszahl(zustandszahl);
     //       GasWerte = gaswerte.get();
     //       return werteRepository.save(gw);
  //  } else {
   //         throw new Exception("Postleitzahl existiert noch nicht im System");
     //   }
     //   }
//}


}
