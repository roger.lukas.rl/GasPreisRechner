DROP TABLE IF EXISTS gas_werte;

CREATE TABLE gas_werte 
(
    postleitzahl Integer (255),
    ort	varchar(300),
    ortsteil varchar(300),
    brennwert double,
    zustandszahl double
);

INSERT INTO gas_werte (postleitzahl, ort, ortsteil, brennwert, zustandszahl) VALUES (10409, 'Berlin', 'Prenzlauer Berg', 11.1, 0.95);DROP TABLE IF EXISTS GasPreiseOrteWerte;

SELECT * FROM gas_werte;